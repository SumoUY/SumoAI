/*
    Sumo|AI - AI header for building AI modules for Sumo|BOT.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|AI.

    Sumo|AI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|AI (2009 - Steven Rodriguez -)                                 //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file sumoai.h
/// \brief Sumo|AI AI module implementation header.
/// \details This is the AI module header for module implementation for the Sumo|BOT program.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//

#include <sumouy.h>

//===========================================================//
//Structures                                                 //
//===========================================================//

struct AIModule
{
    char *Name;
    char *Version;
    char *Vendor;
    SumoUY_UInt8 (*Load)(struct SumoBot *bot, struct Vector2 dohyoCenter, SumoUY_UInt32 dohyoRadius, void (*onMessage)(char *message), SumoUY_UInt8 aiActive);
    void (*ProcessStep)();
    void (*OnSumoBotAIToggle)(SumoUY_UInt8 activated);
    SumoUY_UInt8 (*Close)();
};

//===========================================================//
//Functions                                                  //
//===========================================================//

////////////////////////////////////////////////////
/// \fn struct AIModule *GetModule()
/// \brief Gets the AI module.
/// \return The AI module.
////////////////////////////////////////////////////
struct AIModule *GetModule();
