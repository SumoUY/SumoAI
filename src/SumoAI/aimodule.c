/*
    Sumo|AI - AI header for building AI modules for Sumo|BOT.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|AI.

    Sumo|AI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|AI (2009 - Steven Rodriguez -)                                 //
//=====================================================================//

//===========================================================//
//Libraries                                                  //
//===========================================================//

#include "aimodule.h"

//===========================================================//
//Internal variables                                         //
//===========================================================//

static struct AIModule module;
static struct SumoBot *sumobot;
static struct Vector2 dcenter;
static SumoUY_UInt32 dradius;
static void (*printmessage)(char *message);
static SumoUY_UInt8 aiactivated;

//===========================================================//
//Functions                                                  //
//===========================================================//

struct AIModule *GetModule()
{
    module.Name = "Sumo|AI test module";
    module.Version = "0.1";
    module.Vendor = "Steven Rodriguez";
    module.Load = TestAI_Load;
    module.ProcessStep = TestAI_ProcessStep;
    module.OnSumoBotAIToggle = TestAI_OnSumoBotAIToggled;
    module.Close = TestAI_Close;

    return &module;
}

SumoUY_UInt8 TestAI_Load(struct SumoBot *bot, struct Vector2 dohyoCenter, SumoUY_UInt32 dohyoRadius, void (*onMessage)(char *message), SumoUY_UInt8 aiActive)
{
    //Check for bad parameters
    if((onMessage == NULL) || (bot == NULL))
    {
        return SUMOUY_FALSE;
    }

    //Obtain variables
    sumobot = bot;
    dcenter = dohyoCenter;
    dradius = dohyoRadius;
    printmessage = onMessage;
    aiactivated = aiActive;

    if(SumoUY_BotOnStartFight(sumobot, TestAI_OnStartFight) == SUMOUY_FALSE)
    {
        printmessage("Failed to assign the start fight function");
    }

    if(SumoUY_BotOnStopFight(sumobot, TestAI_OnStopFight) == SUMOUY_FALSE)
    {
        printmessage("Failed to assign the stop fight function");
    }

    if(SumoUY_BotOnReposition(bot, TestAI_OnReposition) == SUMOUY_FALSE)
    {
        printmessage("Failed to assign the reposition function");
    }

    return SUMOUY_TRUE;
}

void TestAI_ProcessStep()
{
    if(aiactivated == SUMOUY_TRUE)
    {
        if(SumoUY_BotProcessData(sumobot) == SUMOUY_FALSE)
        {
            printmessage(SumoUY_GetErrorString());
        }
    }
}

void TestAI_OnSumoBotAIToggled(SumoUY_UInt8 activated)
{
    aiactivated = activated;
}

SumoUY_UInt8 TestAI_Close()
{
    printmessage("AI close");

    return SUMOUY_TRUE;
}

void TestAI_OnStartFight()
{
    if(aiactivated == SUMOUY_TRUE)
    {
        printmessage("Fight started");
        SumoUY_BotSendOK(sumobot);
    }
}

void TestAI_OnStopFight()
{
    if(aiactivated == SUMOUY_TRUE)
    {
        printmessage("Fight stopped");
        SumoUY_BotSendOK(sumobot);
    }
}

void TestAI_OnReposition(struct Vector2 position, SumoUY_UInt16 rotation)
{
    if(aiactivated == SUMOUY_TRUE)
    {
        printmessage("Reposition call");
        SumoUY_BotSendOK(sumobot);
    }
}
