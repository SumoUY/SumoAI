/*
    Sumo|AI - AI header for building AI modules for Sumo|BOT.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|AI.

    Sumo|AI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|AI (2009 - Steven Rodriguez -)                                 //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file sumoai.h
/// \brief Sumo|AI AI module implementation header.
/// \details This is the AI module header for module implementation for the Sumo|BOT program.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//

#include "sumoai.h"

//===========================================================//
//Functions                                                  //
//===========================================================//

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 TestAI_Load(struct SumoBot *bot, struct Vector2 dohyoCenter, SumoUY_UInt32 dohyoRadius, void (*onMessage)(char *message), SumoUY_UInt8 aiActive)
/// \brief Function to load the AI module.
/// \param sumoBot The bot to use.
/// \param dohyoCenter The center of the dohyo.
/// \param dohyoRadius The radius of the dohyo.
/// \param onMessage Function to print messages.
/// \param aiActive SUMOUY_TRUE if Sumo|BOT AI system is active or SUMOUY_FALSE if not.
/// \return SUMOUY_TRUE if loads, SUMOUY_FALSE if not.
////////////////////////////////////////////////////
SumoUY_UInt8 TestAI_Load(struct SumoBot *bot, struct Vector2 dohyoCenter, SumoUY_UInt32 dohyoRadius, void (*onMessage)(char *message), SumoUY_UInt8 aiActive);

////////////////////////////////////////////////////
/// \fn void TestAI_ProcessStep()
/// \brief Function to process the AI module.
////////////////////////////////////////////////////
void TestAI_ProcessStep();

////////////////////////////////////////////////////
/// \fn void TestAI_OnSumoBotAIToggled(SumoUY_UInt8 activated)
/// \brief Function to retrieve the state of the AI system in the Sumo|BOT program.
/// \param activated SUMOUY_TRUE if active and SUMOUY_FALSE if not.
////////////////////////////////////////////////////
void TestAI_OnSumoBotAIToggled(SumoUY_UInt8 activated);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 TestAI_Close()
/// \brief Function to close the AI module.
/// \return SUMOUY_TRUE if closes correctly and SUMOUY_FALSE if not.
////////////////////////////////////////////////////
SumoUY_UInt8 TestAI_Close();

////////////////////////////////////////////////////
/// \fn void TestAI_OnStartFight()
/// \brief Function called when the fight starts.
////////////////////////////////////////////////////
void TestAI_OnStartFight();

////////////////////////////////////////////////////
/// \fn void TestAI_OnStopFight()
/// \brief Function called when the fight stops.
////////////////////////////////////////////////////
void TestAI_OnStopFight();

////////////////////////////////////////////////////
/// \fn void TestAI_OnReposition(struct Vector2 position, SumoUY_UInt16 rotation)
/// \brief Function called when the server says to reposition.
/// \param position The new position.
/// \param rotation The new rotation.
////////////////////////////////////////////////////
void TestAI_OnReposition(struct Vector2 position, SumoUY_UInt16 rotation);
