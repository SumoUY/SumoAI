################################
#Makefile for Sumo|AI
################################

CC = gcc
SRC_DIR = src/SumoAI/
BIN_DIR = bin/
SDL_CONFIG = `sdl-config --cflags --libs`
GLIB_CONFIG = `pkg-config --cflags --libs glib-2.0`
SUMOUY_CONFIG = `pkg-config --cflags --libs sumouy`

all:
	#Compile program
	echo "Compiling Sumo|AI..."
	$(CC) $(SDL_CONFIG) $(GLIB_CONFIG) $(SUMOUY_CONFIG) -o $(BIN_DIR)testai.ai -fPIC -shared -lSDL_ttf -lSDL_gfx -lSDL_image -lSDL_net $(SRC_DIR)aimodule.c	
clean:
	#Cleans compiling thrash
	echo "Cleaning thrash..."
	rm -r -f $(BIN_DIR)*
